#Download Slim base image Python
FROM python:slim

# Create unprivileged user,
# change to the new user 
# and set docker workdir to user $HOME
RUN adduser sgpt
USER sgpt
WORKDIR /home/sgpt

# Create Python Virtual Env named 'sgpt' in '/home/sgpt'
RUN python -m venv $HOME

# Set up Environmental params
ENV PATH="$PATH:$HOME/.local/bin:$HOME/bin" \
    # Add ShellGPT config
    OPENAI_API_KEY=your-OpenAI-API-key-here \
    OPENAI_API_HOST="https://api.openai.com" \
    CHAT_CACHE_LENGTH=100 \
    CHAT_CACHE_PATH=/tmp/sgpt/chat_cache \
    CACHE_LENGTH=100 \
    CACHE_PATH=/tmp/sgpt/cache \
    REQUEST_TIMEOUT=60

# Install PIP and Shell GPT inside Python venv
RUN $HOME/bin/pip install pip --upgrade && $HOME/bin/pip install shell-gpt

# BASH Script to create interactive prompt around the Shell GPT
COPY sgpt.sh /home/sgpt/bin/sgpt.sh

VOLUME /tmp/sgpt

ENTRYPOINT ["/home/sgpt/bin/sgpt.sh"]
