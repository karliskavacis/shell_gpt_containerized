#!/bin/bash

# Activate Python venv
source $HOME/bin/activate

printf "Hello! Welcome to Shell GPT!\nType 'exit' to exit this prompt.\n\n"

while true; do
  read -rp "ShellGPT> " userInput
  case $userInput in
    "exit") exit;;
    *) eval sgpt $userInput;;
  esac
done
